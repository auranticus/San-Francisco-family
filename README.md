# San Francisco family (SF family)

![](https://i.imgur.com/Q5aBQFz.png)

All of the fonts in San Francisco font family, developed by Apple. Including new **SF Camera** font from iOS 13!  
Can be opened on Windows.

For [**SF Hello**](README.md#SF-Hello) font, currently it ~~is impossible to obtain~~ can be obtained only if you are an Apple employee or an authorized Apple reseller.

## Font list
- [SF Camera](/SF%20Camera)
- [SF Cash](/SF%20Cash)
- [SF Compact](/SF%20Compact)
- [SF Compact Rounded](/SF%20Compact%20Rounded)
- [SF Condensed](/SF%20Condensed)
- [SF Condensed Photos](/SF%20Condensed%20Photos)
- SF Hello (not available here as full version)
- [SF Mono](/SF%20Mono) (also in [TTF version](/TrueType%20versions))
- [SF Pro](/SF%20Pro) (also in [TTF version](/TrueType%20versions))
- [SF Pro Rounded](/SF%20Pro%20Rounded) (also in [TTF version](/TrueType%20versions))
- [SF Shields](/SF%20Shields)
- [Serif](/SF%20Serif) (now [*New York*](../../New-York-fonts))

## Wiki
In `Wiki` tab there are some instructions how to change the font metadata to work in Office app on Windows.

## License
Apple fonts are not meant to be used for commercial purposes outside Apple products. The Apple font license can be found [here](https://github.com/windyboy1704/SFPro-JP/blob/master/license.md).

For the **SF Hello** typeface, it is restricted to Apple employees, and permitted contractors and vendors.  

## Notice
Every font in this repository belongs to [Apple](https://www.apple.com). The purpose of this repository is to demonstrate the San Francisco font family, and under no circumstances is it used for commercial purposes.

## SF Hello
This is a rare typeface that only appears in internal Apple presentation and several licensed affiliates’s PDF. In order to obtain **SF Hello** as a non-Apple employee, you must be a member of [Apple Sales Website](https://asw.apple.com) - which is impossible for regular users.
 
`Copyright © 2016-2017 Apple Inc.  All rights reserved. Use of the SF Hello font is restricted to Apple employees and permitted contractors and vendors and may only be used for the purpose of creating Apple-produced communications.`  
Due to its license I am not able to upload the font here.
